class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){

        this.posicionInicial=[4.751025,-74.116408];
        this.escalaInicial=14;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20
        };


        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }


    colocarMarcador(posicion){

        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }
   
    colocarPoligono(configuracion){   

        this.poligonos.push(L.polygon(configuracion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }


}

let miMapa=new Mapa();

miMapa.colocarMarcador([4.751645,-74.114494]);
miMapa.colocarCirculo([4.751645,-74.114494
], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 200
});

miMapa.colocarMarcador([4.752372,-74.117092]);

miMapa.colocarCirculo([4.752372,-74.117092], {
    color: 'blue',
    fillColor: 'blue',
    fillOpacity: 0.5,
    radius: 200
});


miMapa.colocarMarcador([4.756115,-74.111209]);

miMapa.colocarCirculo([4.756115,-74.111209], {
    color: 'black',
    fillColor: 'black',
    fillOpacity: 0.5,
    radius: 200
    
   
});

miMapa.colocarMarcador([4.749486,-74.116826]);

miMapa.colocarCirculo([4.749486,-74.116826], {
    color: 'white',
    fillColor: 'white',
    fillOpacity: 0.5,
    radius: 200
});